#
# Be sure to run `pod lib lint KYModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'KYModel'
  s.version          = '0.1.1'
  s.summary          = 'KYModel this is a High performance model framework for iOS/OSX.'
  s.homepage         = 'http://git.oschina.net/kingly09/KYModel'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'kingly09' => 'kingly09@gmail.com' }
  s.source           = { :git => 'http://git.oschina.net/kingly09/KYModel.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.source_files  = 'KYModel/**/*'
  #s.public_header_files = 'KYModel/*.{h}'
  s.requires_arc = true
  s.frameworks = 'Foundation','UIKit'

end
