//
//  MZRedsUserModel.h
//  MZGogalApp
//
//  Created by kingly on 2017/8/8.
//  Copyright © 2017年 MZGogalApp  Software Shenzhen MeiZhi Interactive Technology Co., Ltd. inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZUserInfo.h"

typedef NS_ENUM(int, REDS_USER_TYPE) {
    REDS_USER_TYPE_HOT      = 1001,     //热榜红人
    REDS_USER_TYPE_BUSINESS = 1002,     //电商红人
    REDS_USER_TYPE_LIVE     = 1003,     //直播红人
    REDS_USER_TYPE_NEW      = 1004      //新晋红人
};


@protocol MZRedsUserModel <NSObject>

@end
/**
 红人对象
 */
@interface MZRedsUserModel : NSObject

@property (nonatomic,assign) long createTime;           //创建时间
@property (nonatomic,strong) NSArray *fansList;         //该红人的3个粉丝列表
@property (nonatomic,assign) int  fansnum;              //该红人的总粉丝的数量
@property (nonatomic,assign) int isIntro;               //默认为 0未关注,1为关注
@property (nonatomic,assign) int livenum;               //该红人直播的数量
@property (nonatomic,assign) int praisenum;             //该红人被点赞的总数量
@property (nonatomic,strong) MZUserInfo *user;          //该红人的详细信息
@property (nonatomic,assign) int userId;                //用户id
@end
