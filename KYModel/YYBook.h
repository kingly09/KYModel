//
//  YYBook.h
//  
//
//  Created by kingly on 2017/8/29.
//
//

#import <Foundation/Foundation.h>
#import "YYAuthor.h"
@interface YYBook : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSUInteger pages;
@property (nonatomic, strong) YYAuthor *author;
@property (nonatomic, assign) NSArray *fansList;
@end
