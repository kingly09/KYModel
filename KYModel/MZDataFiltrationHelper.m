//
//  MZDataFiltrationHelper.m
//  MZGogalApp
//
//  Created by CocoaRoom on 16/8/30.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import "MZDataFiltrationHelper.h"

@implementation MZDataFiltrationHelper

static MZDataFiltrationHelper *sharedObj = nil; //第一步：静态实例，并初始化。
/**
 * @brief 返回实例
 */
+ (MZDataFiltrationHelper*) sharedInstance  //第二步：实例构造检查静态实例是否为nil
{
    @synchronized (self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [[self alloc] init];
        }
    }
    return sharedObj;
}

/**
 * 返回NSString类型的字典成员
 */
-(NSString *)isNullObjwithNSString:(id)object{
     
    NSString *str = (NSString *)object;
    str = [NSString stringWithFormat:@"%@",str];
    if ([self isBlankString:str]){
        return @"";
    }else{
        return str;
    }
}
/**
 * @brief 判断字符串为空和只为空格解决办法
 */
- (BOOL)isBlankString:(NSString *)string
{
    if (string == nil) {
        
        return YES;
        
    }
    
    if (string == NULL) {
        
        return YES;
        
    }
    
    if ([string isKindOfClass:[NSNull class]]) {
        
        return YES;
        
    }
  
  if ([string isEqual:[NSNull null]]) {
    
    return YES;
    
  }
  
  if (string.length > 0) {
    if ([[string lowercaseString] isEqualToString:@"<null>"]) {
      return YES;
    }
    
    if ([[string lowercaseString] isEqualToString:@"(null)"]) {
      return YES;
    }
  }
  //去除两端的空格
  if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0) {
    
    return YES;
    
  }
  
  return NO;
}
/**
 * 返回unsigned longt类型的字典成员
 */
-(long)isNullObjwithLong:(id)object{

    if ([self isNullObj:object]) {
        return 0;
    }else{
        return [object longValue];
    }
}
/**
 * 返回unsigned long long类型的字典成员
 */
-(long long)isNullObjwithLongLong:(id)object{
    
    if ([self isNullObj:object]) {
        return 0;
    }else{
        return [object longLongValue];
    }
}

/**
 * 返回unsigned Int类型的字典成员
 */
-(unsigned int )isNullObjwithInt:(id)object{
    if ([self isNullObj:object]){
        return 0;
    }else{
        return [object unsignedIntValue];
    }
}
/**
 * 返回NSDictionary类型的字典成员
 */
-(NSDictionary *)isNullObjwithNSDictionary:(id)object{
    if ([self isNullObj:object]){
        return nil;
    }else{
        return object;
    }
}


/**
 * 判断NSObject是否为空null、nil...
 * 返回YES标示数据为空
 */
- (BOOL)isNullObj:(id)object
{
    if (object == nil) {
        
        return YES;
        
    }
    
    if (object == NULL) {
        
        return YES;
        
    }
    
    if ([object isKindOfClass:[NSNull class]]) {
        
        return YES;
        
    }
    
    if ([object isEqual:[NSNull null]]) {
        
        return YES;
        
    }
    
    return NO;
}


@end
