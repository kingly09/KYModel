//
//  MZRedsUserModel.m
//  MZGogalApp
//
//  Created by kingly on 2017/8/8.
//  Copyright © 2017年 MZGogalApp  Software Shenzhen MeiZhi Interactive Technology Co., Ltd. inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE. All rights reserved.
//

#import "MZRedsUserModel.h"
#import "NSObject+YYModel.h"

@implementation MZRedsUserModel

#pragma clang diagnostic ignored "-Wdocumentation"
/**
 * @brief 重写json转换方法
 */
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {

    _fansList = [NSArray modelArrayWithClass:[MZUserInfo class] json:dic[@"fansList"]];

    return YES;
}
@end
