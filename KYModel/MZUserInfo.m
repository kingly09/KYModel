//
//  MZUserInfo.m
//  MZGogalApp
//
//  Created by Cobb on 16/1/25.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import "MZUserInfo.h"
#import "MZDataFiltrationHelper.h"

@implementation MZUserInfo

- (void)encodeWithCoder:(NSCoder *)aCoder
{
  [aCoder encodeInteger:_userId forKey:@"userId"];
  [aCoder encodeObject:_userName forKey:@"userName"];
  [aCoder encodeInt:_sex forKey:@"sex"];
  [aCoder encodeObject:_headPic forKey:@"headPic"];
  [aCoder encodeObject:_bgPic forKey:@"bgPic"];
  [aCoder encodeInteger:_birthDay forKey:@"birthDay"];
  
  [aCoder encodeObject:_area forKey:@"area"];
  [aCoder encodeObject:_intro forKey:@"intro"];
  [aCoder encodeObject:_address forKey:@"address"];
  [aCoder encodeObject:_contact forKey:@"contact"];
  [aCoder encodeObject:_phoneNum forKey:@"phoneNum"];
  
  [aCoder encodeInt:_fansNum forKey:@"fansNum"];
  [aCoder encodeInt:_praiseNum forKey:@"praiseNum"];
  [aCoder encodeInt:_concernNum forKey:@"concernNum"];
  [aCoder encodeInt:_vip forKey:@"vip"];
  [aCoder encodeInt:_vipFrom forKey:@"vipFrom"];
  
  [aCoder encodeInt:_role forKey:@"role"];
  [aCoder encodeInt:_state forKey:@"state"];
  [aCoder encodeInt:_isIntro forKey:@"isIntro"];
  [aCoder encodeInt:_isHavePwd forKey:@"isHavePwd"];
  [aCoder encodeInt:_balance forKey:@"balance"];
  
  
  [aCoder encodeInt:_balance forKey:@"cashBalance"];
  
  [aCoder encodeInt:_ucIsShow forKey:@"ucIsShow"];
  
  [aCoder encodeInt:_productCount forKey:@"productCount"];
  [aCoder encodeInt:_programCount forKey:@"programCount"];
  
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super init];
  if (self)
  {
    _userId        = [aDecoder decodeIntegerForKey:@"userId"];
    _userName      = [aDecoder decodeObjectForKey:@"userName"];
    _sex           = [aDecoder decodeIntForKey:@"sex"];
    
    _headPic      = [aDecoder decodeObjectForKey:@"headPic"];
    _bgPic        = [aDecoder decodeObjectForKey:@"bgPic"];
    _birthDay     = [aDecoder decodeIntegerForKey:@"birthDay"];
    
    _area         = [aDecoder decodeObjectForKey:@"area"];
    _intro        = [aDecoder decodeObjectForKey:@"intro"];
    _address      = [aDecoder decodeObjectForKey:@"address"];
    _contact      = [aDecoder decodeObjectForKey:@"contact"];
    _phoneNum     = [aDecoder decodeObjectForKey:@"phoneNum"];
    
    _fansNum     = [aDecoder decodeIntForKey:@"fansNum"];
    
    _praiseNum   = [aDecoder decodeIntForKey:@"praiseNum"];
    _concernNum  = [aDecoder decodeIntForKey:@"concernNum"];
    _vip         = [aDecoder decodeIntForKey:@"vip"];
    _vipFrom     = [aDecoder decodeIntForKey:@"vipFrom"];
    
    
    _role        = [aDecoder decodeIntForKey:@"role"];
    _state       = [aDecoder decodeIntForKey:@"state"];
    _isIntro     = [aDecoder decodeIntForKey:@"isIntro"];
    _isHavePwd   = [aDecoder decodeIntForKey:@"isHavePwd"];
    _balance     = [aDecoder decodeIntForKey:@"balance"];
    
    _cashBalance = [aDecoder decodeIntForKey:@"cashBalance"];
    
    _ucIsShow = [aDecoder decodeIntForKey:@"ucIsShow"];
    
    _productCount        = [aDecoder decodeIntForKey:@"productCount"];
    _programCount        = [aDecoder decodeIntForKey:@"programCount"];
    
    
  }
  return self;
}


- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
  
  _userName = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"userName"]];
  _headPic = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"headPic"]];
  _bgPic = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"bgPic"]];
  _area = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"area"]];
  _intro = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"intro"]];
  _address = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"address"]];
  _contact = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"contact"]];
  _phoneNum = [[MZDataFiltrationHelper sharedInstance] isNullObjwithNSString:[dic objectForKey:@"phoneNum"]];
  
  return YES;
}

@end
