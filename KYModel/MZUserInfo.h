//
//  MZUserInfo.h
//  MZGogalApp
//
//  Created by Cobb on 16/1/25.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MZBaseModel.h"


typedef NS_ENUM(int, USER_VIP_FROM) {
    USER_VIP_FROM_NONE     = 0,     //个人
    USER_VIP_FROM_REDS     = 1,     //红人
    USER_VIP_FROM_SHANJIA  = 2,     //商家
    USER_VIP_FROM_QIEYE    = 3,     //企业
    USER_VIP_FROM_GUANFAN  = 4      //官方认证
};

@protocol MZUserInfo <NSObject>

@end
/**
 * @class MZUserInfo 用户信息的模型
 */
@interface MZUserInfo : MZBaseModel<NSCoding>

//角色
//#define ROLE_GUEST=0;//观众
//#define ROLE_SALER=1;//买手

@property (nonatomic,assign) long userId;//用户id, 由后台分配
@property (nonatomic,copy)   NSString *userName;//用户昵称
@property (nonatomic,assign) int sex;//1男,2女
@property (nonatomic,assign) long authentication;//是否认证
@property (nonatomic,copy)   NSString *headPic;//用户头像
@property (nonatomic,copy)   NSString *bgPic;//背景，保留
@property (nonatomic,assign) long birthDay;//生日,保留
@property (nonatomic,copy)   NSString *area;//所在区域
@property (nonatomic,copy)   NSString *intro;//个人介绍
@property (nonatomic,copy)   NSString *address;//联系地址
@property (nonatomic,copy)   NSString *contact;//联系方式
@property (nonatomic,copy)   NSString *phoneNum; //电话

@property (nonatomic,assign) int praiseNum;      //点赞数 （给别人节目点赞的次数）
@property (nonatomic,assign) int fansNum;        //粉丝数目
@property (nonatomic,assign) int concernNum;     //关注数 （关注别人的次数）
@property (nonatomic,assign) int vip;            //加V (0:非Vip , 1:是VIP)
@property (nonatomic,assign) USER_VIP_FROM vipFrom;        //加V类型（0.个人  1.红人  2.商家   3.企业   4.官方认证）
@property (nonatomic,strong) NSArray *vipRemarks;//加V说明

@property (nonatomic,assign) int role;       //用户角色 （0无直播权限，1，为有直播上传商品的权限， －1 为游客）
@property (nonatomic,assign) int state;      //用户状态，0正常，1封号

@property (nonatomic,assign) int isIntro;    //默认为 0未关注,1为关注
@property (nonatomic,assign) int isHavePwd;  //0.没有支付密码  1.有支付密码
@property (nonatomic,assign) double balance; //余额

@property (nonatomic,assign) double cashBalance; //我的当前余额

@property (nonatomic,assign) int ucIsShow;      //网红卡是否显示,0.不显示,1.显示


@property (nonatomic,assign) int productCount;      //好物数量
@property (nonatomic,assign) int programCount;      //节目数量

@end
