//
//  YYBook.m
//  
//
//  Created by kingly on 2017/8/29.
//
//

#import "YYBook.h"

#import "NSObject+YYModel.h"

@implementation YYBook

/**
 * @brief 重写json转换方法
 */
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {

    _fansList = [NSArray modelArrayWithClass:[YYAuthor class] json:dic[@"fansList"]];
    
    return YES;
}
@end
