//
//  MZDataFiltrationHelper.h
//  MZGogalApp
//
//  Created by CocoaRoom on 16/8/30.
//  Copyright © 2016年 美知互动科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MZDataFiltrationHelper : NSObject


/**
 * @brief 返回实例
 */
+ (MZDataFiltrationHelper *) sharedInstance;


/**
 * 返回NSString类型的字典成员
 */
-(NSString *)isNullObjwithNSString:(id)object;

/**
 * 返回unsigned Int类型的字典成员
 */
-(unsigned int )isNullObjwithInt:(id)object;

/**
 * 返回NSDictionary类型的字典成员
 */
-(NSDictionary *)isNullObjwithNSDictionary:(id)object;

/**
 * 返回unsigned longt类型的字典成员
 */
-(long)isNullObjwithLong:(id)object;

/**
 * 返回unsigned long long类型的字典成员
 */
-(long long)isNullObjwithLongLong:(id)object;


@end
