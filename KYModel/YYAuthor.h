//
//  YYAuthor.h
//  
//
//  Created by kingly on 2017/8/29.
//
//

#import <Foundation/Foundation.h>

@interface YYAuthor : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSDate *birthday;
     
@end
